import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { SubirPage } from '../subir/subir';

// import { AngularFireDatabase } from 'angularfire2/database';
// import { Observable } from 'rxjs/Observable';

import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // posts: Observable<any[]>;
  hayMas: boolean = true

  constructor(private modalCtrl: ModalController,
              public _cap:CargaArchivoProvider) {
    // this.posts = afDB.list('posts').valueChanges();
  }

  mostarModal() {
    let modal = this.modalCtrl.create(SubirPage)
    modal.present()
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    this._cap.cargarImagenes()
      .then( (hayMas:boolean) => {
        this.hayMas = hayMas
        infiniteScroll.complete()
      })
  }

}

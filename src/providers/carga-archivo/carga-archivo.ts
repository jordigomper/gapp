import { Injectable } from '@angular/core';

import { AngularFireDatabase } from "angularfire2/database";
import * as firebase from 'firebase'

import { ToastController } from 'ionic-angular';

import "rxjs/add/operator/map";

@Injectable()
export class CargaArchivoProvider {

  imagenes: ArchivoSubir[] = []
  lastKey:string = null

  constructor(private toastCtrl: ToastController,
              private afDB: AngularFireDatabase) {
    this.cargarUltimoKey()
        .subscribe( () => {
          this.cargarImagenes()
        })
  }

  private cargarUltimoKey() {
    return this.afDB.list('/posts', ref => ref.orderByKey().limitToLast(1))
        .valueChanges()
        .map( (post: any) => {
          this.lastKey = post[0].key

          this.imagenes.push(post[0])
        })
  }

  cargarImagenes() {
    return new Promise((resolve, reject) => {

      this.afDB.list('/posts', 
        ref => ref.limitToLast(3)
                  .orderByKey()
                  .endAt(this.lastKey)
      ).valueChanges()
       .subscribe((posts:any) => {
         posts.pop()
         if(posts.length == 0) {
           console.log("Ya no hay más registros");
           resolve(false)
           return
         }

         this.lastKey = posts[0].key

         for (let i = posts.length-1 ; i >= 0; i--) {
           let post = posts[i]
           this.imagenes.push(post)
         }

         resolve(true)

       })
    })
  }

  cargarImagenFirebase(archivo: ArchivoSubir) {
    let promesa = new Promise((resolve, reject) => {
    
      this.mostrarToast('Cargando...')

      let storeRef = firebase.storage().ref()
      let nombreArchivo:string = new Date().valueOf().toString()

      let uploadTask:firebase.storage.UploadTask =
        storeRef.child(`img/${nombreArchivo}`)
                .putString(archivo.img, 'base64', {contentType: 'image/jpeg'})

                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                
                  ()=>{}, //<- % de Mbs que se han subido
                  error => {
                    //MANEJO DE ERRORES
                    console.log("ERROR AL CARGAR LA IMAGEN");
                    console.log(JSON.stringify(error))
                    this.mostrarToast(JSON.stringify(error))

                    reject()
                  },
                  ()=> {
                    // TODO BIEN
                    console.log("Archivo subido");
                    this.mostrarToast('Imagen cargada correcatamente')

                    let url = uploadTask.snapshot.downloadURL

                    this.crearPost(archivo.titulo, url, nombreArchivo)

                    resolve()
                  }
                
                )
    })
    return promesa
  }

  private crearPost(titulo:string, url:string, nombreArchivo:string) {

    let post: ArchivoSubir = {
      img: url,
      titulo: titulo,
      key: nombreArchivo
    }

    console.log(JSON.stringify(post));

    // this.afDB.list('/posts').push(post)

    this.afDB.object(`/posts/${nombreArchivo}`).update(post)

    this.imagenes.push(post)

  }

  mostrarToast(mensaje:string) {
    this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    }).present();


  }

}

interface ArchivoSubir {
  titulo: string
  img: string
  key?: string
}

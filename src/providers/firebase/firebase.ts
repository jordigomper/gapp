import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyB6cUcYLS6hm05O2710vf7k5USd9bN9myQ",
  authDomain: "gag-ionic-9529f.firebaseapp.com",
  databaseURL: "https://gag-ionic-9529f.firebaseio.com",
  projectId: "gag-ionic-9529f",
  storageBucket: "gag-ionic-9529f.appspot.com",
  messagingSenderId: "14029063916"
};

